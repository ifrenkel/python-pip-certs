variables:
  DS_DISABLE_DIND: "true"
  GEMNASIUM_DB_REF_NAME: "2020-01-15"

stages:
  - build
  - test
  - qa

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - https://gitlab.com/gitlab-org/security-products/ci-templates/raw/master/includes-dev/qa-dependency_scanning.yml

# turn off default analyzer as we're already running it below
gemnasium-python-dependency_scanning:
  only:
    variables:
      - $CI_COMMIT_MESSAGE == "turn on original job"

# normal job with no custom index
# should pass
ds1 - normal job:
  extends: .ds-analyzer
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/tmp:e719917857054257f74e3f7657d8ebda2b89dda9"
  artifacts:
    paths: [gl-ds1.json, qa/expect/gl-ds1.json]
  after_script:
    - cp gl-dependency-scanning-report.json gl-ds1.json
    - cp qa/expect/gl-dependency-scanning-report.json qa/expect/gl-ds1.json

qa-ds1:
  extends: .qa-dependency_scanning
  variables:
    DEPENDENCY_SCANNING_REPORT: "gl-ds1.json"

# job with custom index but no certificate
# should fail with ssl error
ds2 - (should fail) custom index but no cert:
  extends: .ds-analyzer
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/tmp:e719917857054257f74e3f7657d8ebda2b89dda9"
  variables:
    PIP_INDEX_URL: "https://gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal/simple/"
  artifacts:
    paths: [gl-ds2.json, qa/expect/gl-ds2.json]
  after_script:
    - cp gl-dependency-scanning-report.json gl-ds2.json
    - cp qa/expect/gl-dependency-scanning-report.json qa/expect/gl-ds2.json

qa-ds2:
  extends: .qa-dependency_scanning
  script:
    - if [ -f "gl-ds2.json" ]; then echo "this test should have failed but didn't" && exit 1; fi


# job with custom pip_index and pip_cert set
# should pass
ds3 - custom index with pip_cert set:
  extends: .ds-analyzer
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/tmp:e719917857054257f74e3f7657d8ebda2b89dda9"
  variables:
    PIP_INDEX_URL: "https://gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal/simple/"
  artifacts:
    paths: [gl-ds3.json, qa/expect/gl-ds3.json]
  before_script:
    - echo '35.227.149.218 gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal' >> /etc/hosts
    - echo -n | openssl s_client -connect gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/pypi.crt
    - crt=`cat /tmp/pypi.crt`
    - export PIP_CERT="/tmp/pypi.crt"
  after_script:
    - cp gl-dependency-scanning-report.json gl-ds3.json
    - cp qa/expect/gl-dependency-scanning-report.json qa/expect/gl-ds3.json

qa-ds3:
  extends: .qa-dependency_scanning
  variables:
    DEPENDENCY_SCANNING_REPORT: "gl-ds3.json"
    
# job with custom pip_index and additional_ca_cert_bundle set
# should pass
ds4 - custom index with additional_ca_cert_bundle set:
  extends: .ds-analyzer
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/tmp:e719917857054257f74e3f7657d8ebda2b89dda9"
  variables:
    PIP_INDEX_URL: "https://gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal/simple/"
  artifacts:
    paths: [gl-ds4.json, qa/expect/gl-ds4.json]
  before_script:
    - echo '35.227.149.218 gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal' >> /etc/hosts
    - echo -n | openssl s_client -connect gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/pypi.crt
    - crt=`cat /tmp/pypi.crt`
    - export ADDITIONAL_CA_CERT_BUNDLE="$crt"
  after_script:
    - cp gl-dependency-scanning-report.json gl-ds4.json
    - cp qa/expect/gl-dependency-scanning-report.json qa/expect/gl-ds4.json

qa-ds4:
  extends: .qa-dependency_scanning
  variables:
    DEPENDENCY_SCANNING_REPORT: "gl-ds4.json"

# job with custom pip_index and additional_ca_cert_bundle set
# should pass
ds4 - custom index with additional_ca_cert_bundle set:
  extends: .ds-analyzer
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/tmp:e719917857054257f74e3f7657d8ebda2b89dda9"
  variables:
    PIP_INDEX_URL: "https://gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal/simple/"
  artifacts:
    paths: [gl-ds4.json, qa/expect/gl-ds4.json]
  before_script:
    - echo '35.227.149.218 gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal' >> /etc/hosts
    - echo -n | openssl s_client -connect gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/pypi.crt
    - crt=`cat /tmp/pypi.crt`
    - export ADDITIONAL_CA_CERT_BUNDLE="$crt"
  after_script:
    - cp gl-dependency-scanning-report.json gl-ds4.json
    - cp qa/expect/gl-dependency-scanning-report.json qa/expect/gl-ds4.json

qa-ds4:
  extends: .qa-dependency_scanning
  variables:
    DEPENDENCY_SCANNING_REPORT: "gl-ds4.json"

# job with custom pip_index and both pip_cert and additional_ca_cert_bundle set
# should always prefer pip_cert
# should pass
ds5 - custom index with pi_cert and additional_ca_cert_bundle set:
  extends: .ds-analyzer
  image:
    name: "registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/tmp:e719917857054257f74e3f7657d8ebda2b89dda9"
  variables:
    PIP_INDEX_URL: "https://gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal/simple/"
  artifacts:
    paths: [gl-ds5.json, qa/expect/gl-ds5.json]
  before_script:
    - echo '35.227.149.218 gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal' >> /etc/hosts
    - echo -n | openssl s_client -connect gitlab-airgap-pypi.us-west1-b.c.group-secure-a89fe7.internal:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/pypi.crt
    - crt=`cat /tmp/pypi.crt`
    - export PIP_CERT="/tmp/pypi.crt"
    - export ADDITIONAL_CA_CERT_BUNDLE="$crt"
  after_script:
    - cp gl-dependency-scanning-report.json gl-ds5.json
    - cp qa/expect/gl-dependency-scanning-report.json qa/expect/gl-ds5.json

qa-ds5:
  extends: .qa-dependency_scanning
  variables:
    DEPENDENCY_SCANNING_REPORT: "gl-ds5.json"
